import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Forumproc2Component } from './forumproc2.component';

describe('Forumproc2Component', () => {
  let component: Forumproc2Component;
  let fixture: ComponentFixture<Forumproc2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Forumproc2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Forumproc2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
