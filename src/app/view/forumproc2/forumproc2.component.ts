import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/service/course.service';
import { ActivatedRoute } from '@angular/router';
import { UserInfo } from 'src/app/util/user.info';
import { HaveCommentBody } from 'src/app/model/have-comment';
import { ForumComment } from 'src/app/model/forum_comment';
import { ForumCommentService } from 'src/app/service/forum-comment.service';
import { ForumProc } from 'src/app/model/forum_proc_2';


@Component({
  selector: 'app-forum-comment',
  templateUrl: './forumproc2.component.html',
  styleUrls: ['./forumproc2.component.scss']
})
export class ForumProc2Component implements OnInit {
  proc_comment_2: ForumProc[];
  displayedColumns: string[] = ['forum_id','name','comment_id', 'comment_time'];
  constructor(
    private route:ActivatedRoute,
    private forumService:ForumCommentService,
  ) {
   }

  ngOnInit() {
    this.forumService.getProcComment_2().subscribe(
      (x:any)=>{
        this.proc_comment_2 = x;
      },
      (err)=>{
        console.log(err)
      }
    )
  }
}
