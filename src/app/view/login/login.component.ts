import { Component, OnInit, Output, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EventEmitter } from 'events';
import { LoginService } from 'src/app/service/login.service';
import { StudentCreate } from 'src/app/model/studentcreate';
import { TeacherCreate } from 'src/app/model/teachercreate';
import { UserInfo } from 'src/app/util/user.info';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  registerForm: FormGroup;
  isSubmitted  =  false;
  isLoginForm = true;
  error = null;
  sexType: any[]=[
    {value:'male',viewValue:'male'},
    {value:'female',viewValue:'female'},
  ]
  accountType: any[]=[
    {value:'Student',viewValue:'Student'},
    {value:'Teacher',viewValue:'Teacher'},
  ]
  typeSelected :string;
  levelSelect: any[] = [
    {value:'High school',viewValue:'High school'},
    {value:'College',viewValue:'College'},
    {value:'Master',viewValue:'Master'},
  ]
  achivementSelect: any[]=[
    {value:'Fresher',viewValue:'Fresher'},
    {value:'Junior',viewValue:'Junior'},
    {value:'Senior',viewValue:'Senior'},
  ]
  constructor(
    private formBuilder: FormBuilder,
    private loginService : LoginService,
    private router:Router
  ) { }

  ngOnInit() {
    this.loginForm  =  this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      birthdate: ['', Validators.required],
	    accountName:['', Validators.required],
	    sex:['', Validators.required],
	    email:['', Validators.required],
	    address:['', Validators.required],
      accountType:['', Validators.required],
      levelStudent:[''],
      achivement:[''],
      standard:['']
    })
  }
  get formControls() { return this.registerForm.controls; }

  login(){
    let value = this.loginForm.value;
    let username = value.username;
    let password = value.password;
    this.loginService.login(username,password).subscribe((x:any)=>{
      let code = x.code;
      if (code === "200"){
        this.error = null;
        UserInfo.user = x.value;
        this.router.navigate(['/home'])
      }else{
        this.error = x.value;
      }
    }, (err)=>{
      console.log(err);
      
    })
  }

  register(){
    let value = this.registerForm.value;
    if (value.accountType === 'Student'){
      let createStudent = new StudentCreate.Builder()
                  .setAccountType(value.accountType)
                  .setAccountname(value.accountName)
                  .setAchievement(value.achivement)
                  .setAddress(value.address)
                  .setBirthdate(new Date(value.birthdate).toISOString())
                  .setEmail(value.email)
                  .setLevelStudent(value.levelStudent)
                  .setPassword(value.password)
                  .setSex(value.sex)
                  .setUsername(value.username)
                  .build();
      this.loginService.registerStudent(createStudent).subscribe((x:any)=>{
        let code = x.code;
        if (code === "200"){
          this.error = null;
          this.changeForm();
          this.loginForm.setValue({
            username: value.username,
            password: value.password
          });
          window.alert('Register success');
        }else{
          this.error = x.value;
          window.alert(x.value);
        }
      }, (err)=>{
        console.log(err);
      })
    }
    else if (value.accountType === 'Teacher'){
      let createTeacher = new TeacherCreate.Builder()
                  .setAccountType(value.accountType)
                  .setAccountname(value.accountName)
                  .setStandard(value.standard)
                  .setAddress(value.address)
                  .setBirthdate(new Date(value.birthdate).toISOString())
                  .setEmail(value.email)
                  .setPassword(value.password)
                  .setSex(value.sex)
                  .setUsername(value.username)
                  .build();
      this.loginService.registerTeacher(createTeacher).subscribe((x:any)=>{
        let code = x.code;
        if (code === "200"){
          this.error = null;
          this.changeForm();
          this.loginForm.setValue({
            username: value.username,
            password: value.password
          });
          window.alert('Register success');
        }else{
          this.error = x.value;
          window.alert(x.value);
        }
      }, (err)=>{
        console.log(err);
      })
    }
  }

  changeForm(){
    this.isLoginForm = !this.isLoginForm;
    this.registerForm.reset();
  }
}
