import { Component, OnInit } from '@angular/core';
import { ListLessonOfCourseService } from 'src/app/service/list-lesson-of-course.service';
import { ListLesson } from 'src/app/model/listLesson';
@Component({
  selector: 'app-lession-list',
  templateUrl: './lession-list.component.html',
  styleUrls: ['./lession-list.component.scss']
})
export class LessionListComponent implements OnInit {

  list:ListLesson[];
  constructor(private lessonService: ListLessonOfCourseService) { }
  displayedColumns: string[] = ['courseId', 'lessonId', 'courseName', 'lessonName', 'lessonTime'];

  ngOnInit() {
    this.lessonService.seeListLesson(2).subscribe(
      (x:any)=>{
        if (x.code === "200"){
          this.list = x.value; 
        } else if (x.code ==="400"){
          window.alert(x.value);
        }
      },(err)=>{
        console.log(err);
        
      }
    )
  }

}
