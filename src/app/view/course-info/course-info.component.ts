import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/service/course.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfo } from 'src/app/util/user.info';
import { HaveCommentBody } from 'src/app/model/have-comment';
import { ForumComment } from 'src/app/model/forum_comment';
import { ForumCommentService } from 'src/app/service/forum-comment.service';

@Component({
  selector: 'app-course-info',
  templateUrl: './course-info.component.html',
  styleUrls: ['./course-info.component.scss']
})
export class CourseInfoComponent implements OnInit {
  id: number;
  course:any;
  arrComment : ForumComment[];
  constructor(
    private courseService:CourseService,
    private route:ActivatedRoute,
    private forumService:ForumCommentService,
    private router:Router,
  ) {
   }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.courseService.getCourseInfo(this.id).subscribe(
      (x:any)=>{
        this.course = x[0];
        this.getComment();
      }
    )
    
  }

  sendComment(comment:string){
    let userId = 1;
    let courseId = this.id;
    let forumId = this.course.forumId;
    let message = comment;
    let time = new Date();
    let haveComment = new HaveCommentBody(forumId,courseId,userId,time.toISOString(),message);
    this.courseService.postCommentToForum(haveComment).subscribe(
      (x:any)=>{
        if (x.code ==="400"){
          window.alert(x.value)
        }
        this.getComment()
      },(err)=>{
        console.log(err)
      }
    )
  }

  getComment(){
    let forumId = this.course.forumId;
    this.forumService.getForumComment(forumId).subscribe(
      (x:any)=>{
        this.arrComment = x;
        this.arrComment.reverse()
      },(err)=>{
        console.log(err)
      }
    )
  }

  // viewCommentOfUser(id:number){
  //   this.router.navigate()

  // }

}
