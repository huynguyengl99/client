import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/model/category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  listCategories : Category[];
  constructor(
    private route: ActivatedRoute,
    private categoryService:CategoryService
  ) { }

  ngOnInit() {
    this.categoryService.getAllCategories().subscribe(
      (x:any)=>{
        if (x.code ==="200"){
          this.listCategories = x.value;
        }
      },
      (err)=>console.log(err)
    )
  }

}
