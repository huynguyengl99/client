import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/service/category.service';
import {CategoryCourse} from 'src/app/model/category-course';
import {CoursePriceList} from 'src/app/model/coursepricelist';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  list : CategoryCourse[];
  listPrice: CoursePriceList[];
  displayedColumns: string[] = ['id','name', 'created', 'courseLength','courseLevel', 'price','description','teacherId','numOfStudent'];
  displayedColumns1: string[] = ['id','name','courseLength','courseLevel', 'price','CourseAmount'];
  
  constructor(private categoryService:CategoryService) { }
  ngOnInit() {
    this.categoryService.getCourseListOfCategory(1).subscribe(
      (x:any)=>{
        if(x.code === "200"){
          this.list = x.value;
        }
        else if(x.code === "400"){
          window.alert(x.value);
        }
      },(err)=>{
        console.log(err);
      }
    )

    this.categoryService.getCourseListLessThanPrice(1000000).subscribe(
      (x:any)=>{
        if(x.code === "200"){
          this.listPrice = x.value;
        }
        else if(x.code === "400"){
          window.alert(x.value);
        }
      },(err)=>{
        console.log(err);
      }
    )
  }

  FindCost(priceinput:string){
    var numprice = Number(priceinput);
    this.categoryService.getCourseListLessThanPrice(numprice).subscribe(
      (x:any)=>{
        if(x.code === "200"){
          this.listPrice = x.value;
        }
        else if(x.code === "400"){
          window.alert(x.value);
        }
      },(err)=>{
        console.log(err);
      }
    );
  }

  FindCourseOfCategory(indexinput:string){
    var numindex = Number(indexinput);
    this.categoryService.getCourseListOfCategory(numindex).subscribe(
      (x:any)=>{
        if(x.code === "200"){
          this.list = x.value;
        }
        else if(x.code === "400"){
          window.alert(x.value);
        }
      },(err)=>{
        console.log(err);
      }
    );
  }
}

