import { Component, OnInit, Output, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EventEmitter } from 'events';
import { MycourseService } from 'src/app/service/mycourse.service';
import { MyCourseCreate } from 'src/app/model/mycoursecreate';

@Component({
  selector: 'app-my-course',
  templateUrl: './my-course.component.html',
  styleUrls: ['./my-course.component.scss']
})
export class MyCourseComponent implements OnInit {
  mycourseForm : FormGroup;
  erorr = null;
  constructor(
    private formBuilder: FormBuilder,
    private mycourseService : MycourseService
  ){}

  ngOnInit() {
    this.mycourseForm  =  this.formBuilder.group({
      courseId: ['', Validators.required],
      studentId: ['', Validators.required],
      createdDate: ['', Validators.required],
      progress: ['', Validators.required]
  });
  }
  get formControls() { return this.mycourseForm.controls; }
  addToMyCourse(){
    let value = this.mycourseForm.value;
    let addCourse = new MyCourseCreate.Builder()
                        .setCourseId(value.courseId)
                        .setStudentId(value.studentId)
                        .setCreateDate(value.createdDate)
                        .setProgress(value.progress)
                        .build();
  
    this.mycourseService.createMyCourse(addCourse).subscribe((x:any)=>{
      let code = x.code;
      if (code === "200"){
        this.erorr = null;
        window.alert('Success!');
      }else{
        this.erorr = x.value;
        window.alert(x.value);
      }
    }, (err)=>{
      console.log(err);
    })                    
  }
}
