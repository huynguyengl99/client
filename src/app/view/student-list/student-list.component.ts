import { Component, OnInit } from '@angular/core';
import { ListStudentOfCourseService } from 'src/app/service/list-student-of-course.service';
import { ListStudent } from 'src/app/model/listStudent';
@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {
  list:ListStudent[];
  constructor(private studentService: ListStudentOfCourseService) { }
  displayedColumns: string [] = ['id', 'courseId', 'courseName', 'accountName', 'signUpDate'];
  ngOnInit() {
    this.studentService.seeListStudent(2).subscribe(
      (x:any)=>{
        if (x.code === "200"){
          this.list = x.value; 
        } else if (x.code ==="400"){
          window.alert(x.value);
        }
      },(err)=>{
        console.log(err);
        
      }
    )
  }

}
