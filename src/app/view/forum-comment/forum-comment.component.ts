import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/service/course.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfo } from 'src/app/util/user.info';
import { HaveCommentBody } from 'src/app/model/have-comment';
import { ForumComment } from 'src/app/model/forum_comment';
import { ForumCommentService } from 'src/app/service/forum-comment.service';
import { ForumProc1 } from 'src/app/model/forum_proc_1';


@Component({
  selector: 'app-forum-comment',
  templateUrl: './forum-comment.component.html',
  styleUrls: ['./forum-comment.component.scss']
})
export class ForumCommentComponent implements OnInit {
  id: number;
  proc_comment: ForumProc1[];
  displayedColumns : string[] = ['forum_id','name','comment_id', 'comment_time', 'content'];
  constructor(
    private route:ActivatedRoute,
    private forumService:ForumCommentService,
    private router:Router
  ) {
   }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getComment();
  }

  getComment(){
    this.forumService.getProcComment_1(this.id).subscribe(
      (x:any)=>{
        this.proc_comment = x;
      },
      (err)=>{
        console.log(err)
      }
    )
  }

  doNavigate(){
    this.router.navigate([`/procedure_select_1_lau/${this.id}`])
    this.getComment()
  }
}
