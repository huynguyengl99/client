export class HaveCommentBody {
    forum_id: number;
    course_id: number;
    student_id: number;
    comment_time: string;
    content: string;

    constructor(fid,cid,sid,time,content) {
        this.forum_id = fid;
        this.course_id = cid;
        this.student_id = sid;
        this.comment_time = time;
        this.content = content;
    }
}