import { build$ } from 'protractor/built/element';
export class MyCourseCreate{
    courseId: number;
    studentId: number;
    createdDate: Date;
    progress: number;
    constructor(build){
        this.courseId = build.courseId;
        this.createdDate = build.createdDate;
        this.progress = build.progress;
        this.studentId = build.studentId;
    }
    static get Builder(){
        class Builder{
            private courseId: number;
            private studentId: number;
            private createDate: Date;
            private progress:number;
            setCourseId(courseId:any){
                this.courseId = courseId;
                return this;
            }
            setStudentId(studentId:any){
                this.studentId = studentId;
                return this;
            }
            setCreateDate(createdDate:any){
                this.createDate =  createdDate;
                return this
            }
            setProgress(progress:any){
                this.progress = progress;
                return this
            }
            build(){
                return new MyCourseCreate(this);
            }
        }
        return Builder;
    }
}