export class TeacherCreate {
    username: string;
    password: string;
    birthdate: Date;
    accountName: string;
    sex: string;
    email: string;
    address: string;
    accountType: string;
    standard: string;
    constructor(build) {
        this.username = build.username;
        this.password = build.password;
        this.birthdate = build.birthdate;
        this.accountName = build.accountName;
        this.sex = build.sex;
        this.email = build.email;
        this.address = build.address;
        this.accountType = build.accountType;
        this.standard = build.standard;
    }
    static get Builder() {

        class Builder {
            private username: string;
            private password: string;
            private birthdate: Date;
            private accountName: string;
            private sex: string;
            private email: string;
            private address: string;
            private accountType: string;
            private standard: string;
            setUsername(username: any) {
                this.username = username
                return this;
            }
            setPassword(password) {
                this.password = password
                return this;
            }
            setBirthdate(birthdate) {
                this.birthdate = birthdate
                return this;
            }
            setAccountname(accountName) {
                this.accountName = accountName
                return this;
            }
            setSex(sex) {
                this.sex = sex
                return this;
            }
            setEmail(email) {
                this.email = email
                return this;
            }
            setAddress(address) {
                this.address = address
                return this;
            }
            setAccountType(accountType) {
                this.accountType = accountType
                return this;
            }
            setStandard(standard) {
                this.standard = standard
                return this;
            }
            build() {
                return new TeacherCreate(this)
            }
        }
        return Builder;
    }
}