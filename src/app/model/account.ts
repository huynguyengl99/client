export class Account{
    username: string;
    password: string;
    createdAt: Date;
    birthdate: Date;
    accountName: string;
    sex: string;
    email: string;
    address: string;
    accountType: string;
    major: string;
    position: string;
}