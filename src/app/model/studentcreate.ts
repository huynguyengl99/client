export class StudentCreate {
    username: string;
    password: string;
    birthdate: Date;
    accountName: string;
    sex: string;
    email: string;
    address: string;
    accountType: string;
    levelStudent?: string;
    achievement?: string;
    needConsultant?: boolean;
    constructor(build) {
        this.username = build.username;
        this.password = build.password;
        this.birthdate = build.birthdate;
        this.accountName = build.accountName;
        this.sex = build.sex;
        this.email = build.email;
        this.address = build.address;
        this.accountType = build.accountType;
        this.levelStudent = build.levelStudent;
        this.achievement = build.achievement;
        this.needConsultant = build!!.needConsultant;
    }
    static get Builder() {

        class Builder {
            private username: string;
            private password: string;
            private birthdate: Date;
            private accountName: string;
            private sex: string;
            private email: string;
            private address: string;
            private accountType: string;
            private levelStudent: string;
            private achievement: string;
            private needConsultant: boolean;
            setUsername(username: any) {
                this.username = username
                return this;
            }
            setPassword(password) {
                this.password = password
                return this;
            }
            setBirthdate(birthdate) {
                this.birthdate = birthdate
                return this;
            }
            setAccountname(accountName) {
                this.accountName = accountName
                return this;
            }
            setSex(sex) {
                this.sex = sex
                return this;
            }
            setEmail(email) {
                this.email = email
                return this;
            }
            setAddress(address) {
                this.address = address
                return this;
            }
            setAccountType(accountType) {
                this.accountType = accountType
                return this;
            }
            setLevelStudent(levelStudent) {
                this.levelStudent = levelStudent
                return this;
            }
            setAchievement(achievement) {
                this.achievement = achievement
                return this;
            }
            build() {
                return new StudentCreate(this)
            }
        }
        return Builder;
    }
}