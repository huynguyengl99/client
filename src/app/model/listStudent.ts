export class ListStudent{
    id: number;
    courseId: number;
    courseName: string;
    accountName: string;
    signUpDate: string;
}