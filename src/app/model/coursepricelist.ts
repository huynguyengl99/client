export class CoursePriceList{
    id: number;
    name: string;
    courseLength: number;
    courseLevel: string;
    price: number;
    CourseAmount: number;
}