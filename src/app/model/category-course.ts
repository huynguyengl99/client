export class CategoryCourse{
    id:number;
    name: string;
    created: Date;
    courseLength: number;
    courseLevel: string;
    price: number;
    description: string;
    teacherId: number;
    numOfStudent: number;
}