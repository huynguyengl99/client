export class Category {
    id: number;
    info: string;
    name: string;
    courseCount: number;
}