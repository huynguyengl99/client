export class ListLesson{
        courseId: number;
        lessonId: number;
        courseName: string;
        lessonName: string;
        lessonTime: number;
}