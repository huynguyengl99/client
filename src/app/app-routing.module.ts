import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CounselComponent } from './view/counsel/counsel.component';
import { LoginComponent } from './view/login/login.component';
import { HomeComponent } from './view/home/home.component';
import { MyCourseComponent } from './view/my-course/my-course.component';
import { MyAccountComponent } from './view/my-account/my-account.component';
import { CourseInfoComponent } from './view/course-info/course-info.component';
import { ForumCommentComponent } from './view/forum-comment/forum-comment.component';
import { ForumProc2Component } from './view/forumproc2/forumproc2.component';
import { CategoryComponent } from './view/category/category.component';
import { StudentListComponent } from './view/student-list/student-list.component';
import { LessionListComponent } from './view/lession-list/lession-list.component';


const routes: Routes = [
  { path: 'counsel', component: CounselComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'mycourses', component: MyCourseComponent },
  { path: 'myaccount', component: MyAccountComponent },
  { path: 'courseinfo/:id', component: CourseInfoComponent},
  { path: 'procedure_select_1_lau/:id', component: ForumCommentComponent},
  { path: 'procedure_select_2_lau', component: ForumProc2Component},
  { path: 'category/:id', component:CategoryComponent},
  { path: 'studentList/:id', component:StudentListComponent},
  { path: 'lessonList/:id', component:LessionListComponent},


  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
