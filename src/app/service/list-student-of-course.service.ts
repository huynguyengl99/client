import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ListStudentOfCourseService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(private http: HttpClient) { }
  seeListStudent(courseId:number){
    return this.http.get(`http://127.0.0.1:3000/students/${courseId}`,this.httpOptions)

  }
}
