import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }
  getCourseListOfCategory(categoryId:number){
    return this.http.get(`http://127.0.0.1:3000/Category/${categoryId}`,this.httpOptions)
  }
  getCourseListLessThanPrice(price:number){
    return this.http.get(`http://127.0.0.1:3000/Course/${price}`,this.httpOptions)
  }
  getAllCategories(){
    return this.http.get(`http://127.0.0.1:3000/categories`,this.httpOptions);
  }
}
