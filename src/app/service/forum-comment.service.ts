import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ForumCommentService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }
  getForumComment(id:number){
    return this.http.get(`http://127.0.0.1:3000/forum-comment/${id}`,this.httpOptions)
  }
  getProcComment_1(id:number){
      return this.http.get(`http://127.0.0.1:3000/havecomment/${id}`,this.httpOptions)
  }
  getProcComment_2(){
    return this.http.get(`http://127.0.0.1:3000/havecomment/procedure`,this.httpOptions)
}
}
