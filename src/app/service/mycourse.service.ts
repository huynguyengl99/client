import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MyCourseCreate } from '../model/mycoursecreate';

@Injectable({
  providedIn: 'root'
})
export class MycourseService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }
  createMyCourse(mycourse:MyCourseCreate){
    return this.http.post('http://[::1]:3000/mycourse',mycourse,this.httpOptions)
  }
}
