import { TestBed } from '@angular/core/testing';

import { Forumproc2Service } from './forumproc2.service';

describe('Forumproc2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Forumproc2Service = TestBed.get(Forumproc2Service);
    expect(service).toBeTruthy();
  });
});
