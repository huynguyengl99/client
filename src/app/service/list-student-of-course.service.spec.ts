import { TestBed } from '@angular/core/testing';

import { ListStudentOfCourseService } from './list-student-of-course.service';

describe('ListStudentOfCourseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListStudentOfCourseService = TestBed.get(ListStudentOfCourseService);
    expect(service).toBeTruthy();
  });
});
