import { TestBed } from '@angular/core/testing';

import { MycourseService } from './mycourse.service';

describe('MycourseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MycourseService = TestBed.get(MycourseService);
    expect(service).toBeTruthy();
  });
});
