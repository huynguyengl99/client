import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HaveCommentBody } from '../model/have-comment';


@Injectable({
  providedIn: 'root'
})
export class CourseService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }
  getCourseInfo(id:number){
    return this.http.get(`http://127.0.0.1:3000/course/${id}`,this.httpOptions)
  }
  postCommentToForum(haveComment:HaveCommentBody){
    return this.http.post(`http://127.0.0.1:3000/havecomments`,haveComment,this.httpOptions);
  }
}
