import { Injectable } from '@angular/core';
import { LoginInfo } from '../model/logininfo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StudentCreate } from '../model/studentcreate';
import { TeacherCreate } from '../model/teachercreate';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }
  login(username:string, password: string){
    let loginInfo = new LoginInfo(username,password);
    return this.http.post('http://127.0.0.1:3000/accounts/login',loginInfo,this.httpOptions)
  }
  registerStudent(student:StudentCreate){
    return this.http.post('http://127.0.0.1:3000/students',student,this.httpOptions)
  }
  registerTeacher(teacher:TeacherCreate){
    return this.http.post('http://127.0.0.1:3000/teachers',teacher,this.httpOptions)
  }
}
