import { TestBed } from '@angular/core/testing';

import { ForumCommentService } from './forum-comment.service';

describe('ForumCommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForumCommentService = TestBed.get(ForumCommentService);
    expect(service).toBeTruthy();
  });
});
