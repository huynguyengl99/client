import { TestBed } from '@angular/core/testing';

import { ListLessonOfCourseService } from './list-lesson-of-course.service';

describe('ListLessonOfCourseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListLessonOfCourseService = TestBed.get(ListLessonOfCourseService);
    expect(service).toBeTruthy();
  });
});
