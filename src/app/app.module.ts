import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CounselComponent } from './view/counsel/counsel.component';
import { LoginComponent } from './view/login/login.component';
import { HomeComponent } from './view/home/home.component';
import {MatTableModule} from '@angular/material/table'
import {
  MatCardModule,
  MatInputModule,
  MatButtonModule
} from '@angular/material';
import { FormsModule }   from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { StudentComponent } from './view/student/student.component';
import { HttpClientModule } from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { MyCourseComponent } from './view/my-course/my-course.component';
import { MyAccountComponent } from './view/my-account/my-account.component';
import { CourseInfoComponent } from './view/course-info/course-info.component';
import { ForumCommentComponent } from './view/forum-comment/forum-comment.component';
import { ForumProc2Component } from './view/forumproc2/forumproc2.component';
import { CategoryComponent } from './view/category/category.component';
import { LessionListComponent } from './view/lession-list/lession-list.component';
import { StudentListComponent } from './view/student-list/student-list.component';
@NgModule({
  declarations: [
    AppComponent,
    CounselComponent,
    LoginComponent,
    HomeComponent,
    StudentComponent,
    MyCourseComponent,
    MyAccountComponent,
    CourseInfoComponent,
    ForumCommentComponent,
    ForumProc2Component,
    CategoryComponent,
    LessionListComponent,
    StudentListComponent
  ],
  imports: [
    MatTableModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
